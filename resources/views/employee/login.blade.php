@extends('layouts.login')
@section('content')
<div class="login-box">
  <div class="login-logo">
    <a href="/" style="color:white;"><b>Employee airBill Plus</b> App</a>
  </div>
  <!--    <div class="" style="text-align: center;">
        <img src="dist/img/logo3_new.png" class="img-circle" alt="User Image">
    </div>-->
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Log in to start your session</p>

    <form method="POST" action="{{ url('employee-login') }}" aria-label="{{ __('Login') }}">
      @csrf
      <div class="form-group has-feedback {{ $errors->has('mobile_no') ? ' has-error' : '' }}">
        <input type="text" id="mobile_no" name="mobile_no" class="form-control number" placeholder="Mobile"
          value="{{ Cookie::get('mobile_no') }}" maxlength="10"> <span
          class="glyphicon glyphicon-earphone form-control-feedback"></span>
        @if ($errors->has('email'))
        <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
      </div>
      <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
        <input type="password" class="form-control" placeholder="Password" id="password" name="password"
          value="{{ Cookie::get('password') }}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
        <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
      </div>
      <!--            <div class="form-group row">
            <div class="col-md-6 offset-md-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
        </div>-->
      <!--                             <div class="row">
                <div class="col-lg-6">
               <div class="input-group">
                        <span class="input-group-addon">
                          <input type="radio" name="role" value="1">
                        </span>
                    <input type="text" class="form-control" value="Admin" readonly>
                  </div>
                   /input-group 
                </div>
                 /.col-lg-6 
                <div class="col-lg-6">
                  <div class="input-group">
                        <span class="input-group-addon">
                          <input type="radio" name="role" value="2">
                        </span>
                    <input type="text" class="form-control" value="Employee" readonly>
                  </div>
                   /input-group 
                </div>
                 /.col-lg-6 
              </div>-->
      <!--<br/>-->
      <!--                             <div class="form-group has-feedback">
                                 <input type="text" class="form-control" placeholder="CID" class="cid" id="cid" name="cid" onkeyup="check(this.value);" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        
      </div>-->
      <div class="form-group has-feedback">
        <!--        <select class="form-control select2" style="width: 100%;" name="item_category" required>
                        <option value="">-- Select Location -- </option>
                        <option value="1">Pune</option>
                        <option value="2">Solapur</option>-->
        </select>

      </div>

      <!-- /.row -->
      <div class="row">
        <!--        <div class="col-xs-8">
            <div class="checkbox icheck">
                <label>
                    <input type="checkbox"> Remember Me
                </label>
            </div>
        </div>-->
      </div>
      <div class="row">
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Log In</button>
        </div>
        <div class="col-xs-6">
          <a href="{{url('/')}}" class="btn btn-primary btn-block btn-flat">Admin Login</a>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!--    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>-->
    <!-- /.social-auth-links -->

    <!--<a href="#">I forgot my password</a>
    --><br>
    <!--    <div style="text-align:center;">
  </div>
  <!-- /.login-box-body -->
  </div>
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script>
     $(document).ready(function(){
   $('.number').keypress(function(event) {
                    var $this = $(this);
                    if ((event.which != 46 || $this.val().indexOf('.') != - 1) &&
                            ((event.which < 48 || event.which > 57) &&
                                    (event.which != 0 && event.which != 8))) {
                    event.preventDefault();
                    }

                    var text = $(this).val();
                    if ((event.which == 46) && (text.indexOf('.') == - 1)) {
                    setTimeout(function() {
                    if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                    }
                    }, 1);
                    }

                    if ((text.indexOf('.') != - 1) &&
                            (text.substring(text.indexOf('.')).length > 2) &&
                            (event.which != 0 && event.which != 8) &&
                            ($(this)[0].selectionStart >= text.length - 2)) {
                    event.preventDefault();
                    }
    });
     $('.number').bind("paste", function(e) {
                    var text = e.originalEvent.clipboardData.getData('Text');
                    if ($.isNumeric(text)) {
                    if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > - 1)) {
                    e.preventDefault();
                    $(this).val(text.substring(0, text.indexOf('.') + 3));
                    }
                    }
                    else {
                    e.preventDefault();
                    }
    });
   });
    function check(cid)
    {
            $.ajax({
        url: 'check_location',
        type: "get",
        data: {cid:cid},
        success: function(reportdata) { 
                alert(reportdata);
            }
        });
    }
  </script>
  @endsection

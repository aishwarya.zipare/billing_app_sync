@extends('layouts.login')

@section('content')
<style>
    .error{
        color:red;
    }
    .has-feedback label~.form-control-feedback {
    top: 3px;
}
</style>
<section class="content">
    
  <div class="register-box">
  <div class="register-logo">
    <a href="/" style="color:white;"><b>Billing</b> App</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form  id="register_form" method="POST" action="{{ url('client-register') }}" aria-label="{{ __('Register') }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <span id="lblError" style="color: red"></span>
      <div class="form-group has-feedback">
        <input type="text" name="reg_companyname" id="reg_companyname" class="form-control special_char" placeholder="Company Name" required >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
          <input type="text" name="reg_personname" id="reg_personname" class="form-control special_char" placeholder="Contact Person Name" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
          <input type="text" name="reg_mobileno" onkeypress="return phoneno(event)" id="reg_mobileno" class="form-control number" placeholder="Mobile" required>
        <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
          <input type="email" name="reg_emailid" id="reg_emailid" class="form-control" placeholder="Email Address" >
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
          <input type="text" name="reg_address" id="reg_address" class="form-control" placeholder="Address">
        <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
      </div>
      <div class="form-group">
        <label class="">
            <div class="iradio_minimal-blue checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="location" class="minimal" value="single" checked="" style="position: absolute; opacity: 0;" required> Single Location</div>
        </label>
        <label class="">
            <div class="iradio_minimal-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="location" class="minimal" value="multiple" style="position: absolute; opacity: 0;" required> Multi Location</div>
        </label>
      </div>
      <div class="form-group has-feedback">
          <input type="text" name="reg_username" id="reg_username" class="form-control" placeholder="Username" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
          <input type="password" name="reg_userpassword" id="reg_userpassword" class="form-control" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" id="reg_companyid" id="reg_companyid" class="form-control" placeholder="Business" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
          <input type="text" id="reg_dealercode" name="reg_dealercode" class="form-control" placeholder="Dealer Code" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
       <div class="form-group has-feedback">
           <b>Upload Logo</b>
      </div>              
      <div class="form-group has-feedback">
          <input type="file" id="reg_dealercode" name="upload_logo" class="form-control" placeholder="Upload Logo" >
        <span class="glyphicon glyphicon-folder-open form-control-feedback"></span>
      </div>
                    <div class="form-group" style="display:none;">
        <label class="">
          <div class="icheckbox_minimal-blue checked" aria-checked="false" aria-disabled="false" style="position: relative;">
              <input type="checkbox" class="minimal" name="permission[]" value="1" id="billing" style="position: absolute; opacity: 0;" required> Billing App
          </div>
        </label>
		
<!--        <label class="">
          <div class="icheckbox_minimal-blue" aria-checked="false" aria-disabled="false" style="position: relative;">
              <input type="checkbox" class="minimal" name="permission[]" id="enquiry" value="2" style="position: absolute; opacity: 0;" required> Enquiry App
          </div>
        </label>-->
      </div>
      <div class="row">
        <div class="col-xs-8">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <a href="{{url('/')}}" class="btn btn-primary btn-block btn-flat">Login</a>
        </div>
        <!-- /.col -->
      </div>
    </form>

<!--    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div>-->

    <!--<a href="login.html" class="text-center">I already have a membership</a>-->
  </div>
  <!-- /.form-box -->
</div>
            </div>
        </div>   
    </div>
</section>
 
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
<!--<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js'></script>-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type='text/javascript' src='js/jquery.validate.js'></script>
<script type="text/javascript">
//            $("#btnsubmit").on("click",function(){

            var jvalidate = $("#register_form").validate({
                rules: {   
                        email: {required: true},
                        password : {required: true},
                    },
                     messages: {
                         email: "Please Enter Email Address",
                         password: "Please Enter Password"
                       }  
                });
                $('.number').keypress(function(event) {
                    var $this = $(this);
                    if ((event.which != 46 || $this.val().indexOf('.') != - 1) &&
                            ((event.which < 48 || event.which > 57) &&
                                    (event.which != 0 && event.which != 8))) {
                    event.preventDefault();
                    }

                    var text = $(this).val();
                    if ((event.which == 46) && (text.indexOf('.') == - 1)) {
                    setTimeout(function() {
                    if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                    }
                    }, 1);
                    }

                    if ((text.indexOf('.') != - 1) &&
                            (text.substring(text.indexOf('.')).length > 2) &&
                            (event.which != 0 && event.which != 8) &&
                            ($(this)[0].selectionStart >= text.length - 2)) {
                    event.preventDefault();
                    }
    });
     $('.number').bind("paste", function(e) {
                    var text = e.originalEvent.clipboardData.getData('Text');
                    if ($.isNumeric(text)) {
                    if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > - 1)) {
                    e.preventDefault();
                    $(this).val(text.substring(0, text.indexOf('.') + 3));
                    }
                    }
                    else {
                    e.preventDefault();
                    }
    });
                $('#btnsubmit').on('click', function() {
                    $("#orderForm").valid();
                });
                $(".special_char").keypress(function (e) {
            var keyCode = e.keyCode || e.which;
 
            $("#lblError").html("");
 
            //Regex for Valid Characters i.e. Alphabets and Numbers.
            var regex = /^[A-Za-z0-9\s]+$/;
 
            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $("#lblError").html("Only Alphabets and Numbers allowed.");
            }
 
            return isValid;
        });
		$("#reg_mobileno").focusout(function () {
                    var email = $(this).val();
                    $.ajax({
                        url: 'mobile-validate/' + email,
                        type: "GET",
                        success: function (data) {
                            console.log(data);
                            $("#mobile_validate").html(data);
                            if (data != "") {
                                $("#reg_mobileno").val("");
                            }
                        }
                    });
                });
                
                $("#email").focusout(function () {
                var email = $(this).val();
                $.ajax({
                    url: 'email-validate/' + email,
                    type: "GET",
                    success: function (data) {
                        console.log(data);
                        $("#email_validate").html(data);
                        if (data != "") {
                            $("#email").val("");
                        }
                    }
                });
            });
            
            function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function phoneno(){          
            $('#reg_mobileno').keypress(function(e) {
                var length = jQuery(this).val().length;
       if(length > 11) {
            return false;
       } else if(e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
       } else if((length == 0) && (e.which == 48)) {
            return false;
       }
            });
        }
        </script>
@endsection
